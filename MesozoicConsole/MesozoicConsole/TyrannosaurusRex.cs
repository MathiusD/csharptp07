﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;

namespace TRex
{
    public class TyrannosaurusRex : Dinosaur
    {
        protected override string specie { get { return "TyrannosaurusRex"; } }
        public TyrannosaurusRex(string name, int age) : base(name, age)
        {

        }
        public override string roar()
        {
            return "Uuuuuuuuunnnnnnnnnnnnnnn!!!";
        }
        public override string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans. Je mange les autres Dinos.", this.name, this.specie, this.age);
        }
    }
}
