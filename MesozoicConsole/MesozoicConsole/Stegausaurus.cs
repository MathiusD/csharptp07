﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;

namespace Stego
{
    public class Stegausaurus : Dinosaur
    {
        protected override string specie { get { return "Stegausaurus"; } }
        public Stegausaurus(string name, int age) : base(name, age)
        {

        }
        public override string roar()
        {
            return "Meeeeeeuuuuhhhh";
        }
        public override string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans. Je mange des fougères.", this.name, this.specie, this.age);
        }
    }
}
