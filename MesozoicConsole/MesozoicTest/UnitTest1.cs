﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MesozoicConsole;
using Mesozoic;
using Diplo;
using Stego;
using TRex;
using Trice;
using Dino;

namespace MesozoicTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestStegausaurusConstructor()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Stegausaurus", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestStegausaurusRoar()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Assert.AreEqual("Meeeeeeuuuuhhhh", louis.roar());
        }

        [TestMethod]
        public void TestStegausaurusSayHello()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans. Je mange des fougères.", louis.sayHello());
        }
        public void TestTRexConstructor()
        {
            TyrannosaurusRex damien = new TyrannosaurusRex("Damien", 12);

            Assert.AreEqual("Damien", damien.getName());
            Assert.AreEqual("TyrannosaurusRex", damien.getSpecie());
            Assert.AreEqual(12, damien.getAge());
        }

        [TestMethod]
        public void TestTRexRoar()
        {
            TyrannosaurusRex damien = new TyrannosaurusRex("Damien", 12);
            Assert.AreEqual("Uuuuuuuuunnnnnnnnnnnnnnn!!!", damien.roar());
        }

        [TestMethod]
        public void TestTRexSayHello()
        {
            TyrannosaurusRex damien = new TyrannosaurusRex("Damien", 12);
            Assert.AreEqual("Je suis Damien le TyrannosaurusRex, j'ai 12 ans. Je mange les autres Dinos.", damien.sayHello());
        }
        public void TestDiploConstructor()
        {
            Diplodocus nessie = new Diplodocus("Nessie", 12);

            Assert.AreEqual("Nessie", nessie.getName());
            Assert.AreEqual("Diplodocus", nessie.getSpecie());
            Assert.AreEqual(12, nessie.getAge());
        }

        [TestMethod]
        public void TestDiploRoar()
        {
            Diplodocus nessie = new Diplodocus("Nessie", 12);
            Assert.AreEqual("HmmMMMMmmmmhmhmhmhmhmmmmhmmm", nessie.roar());
        }

        [TestMethod]
        public void TestDiploSayHello()
        {
            Diplodocus nessie = new Diplodocus("Nessie", 12);
            Assert.AreEqual("Je suis Nessie le Diplodocus, j'ai 12 ans. Je mange des feuilles d'arbres.", nessie.sayHello());
        }
        public void TestTriceConstructor()
        {
            Triceratops louis = new Triceratops("Louis", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Triceratops", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestTriceRoar()
        {
            Triceratops louis = new Triceratops("Louis", 12);
            Assert.AreEqual("Ummmmrrrr", louis.roar());
        }

        [TestMethod]
        public void TestTriceSayHello()
        {
            Triceratops louis = new Triceratops("Louis", 12);
            Assert.AreEqual("Je suis Louis le Triceratops, j'ai 12 ans. J'empale les dinos.", louis.sayHello());
        }
        public void TestDinoConstructor()
        {
            Dinosaurs louis = new Dinosaurs("Louis", 12);

            Assert.AreEqual("Louis", louis.getName());
            Assert.AreEqual("Dinosaur", louis.getSpecie());
            Assert.AreEqual(12, louis.getAge());
        }

        [TestMethod]
        public void TestDinoRoar()
        {
            Dinosaurs louis = new Dinosaurs("Louis", 12);
            Assert.AreEqual("Grrr", louis.roar());
        }

        [TestMethod]
        public void TestDinoSayHello()
        {
            Dinosaurs louis = new Dinosaurs("Louis", 12);
            Assert.AreEqual("Je suis Louis le Dinosaur, j'ai 12 ans.", louis.sayHello());
        }
        [TestMethod]
        public void TestDinosaurHug()
        {
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Diplodocus nessie = new Diplodocus("Nessie", 11);

            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie.", louis.hug(nessie));
        }
    }
}
